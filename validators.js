function email(email) {
    const regex = /@/
    return regex.test(email);
}

module.exports = {
    email
}
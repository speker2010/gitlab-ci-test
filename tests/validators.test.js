const validators = require('../validators');

describe("Test email validator", () => {
    it("It's success test", () => {
        expect(validators.email('test@mail.ru')).toBe(true);
    });
    it("It's fail test", () => {
        expect(validators.email('wrong email')).toBe(false);
    });
});